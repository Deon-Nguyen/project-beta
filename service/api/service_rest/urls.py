from django.urls import path
from .views import (
    appointmentList,
    appointmentDetail,
    appointmentCancel,
    appointmentFinish,
    technicianList,
    technicianDetail,
    autoMobileList,
    )

urlpatterns = [
    path("appointments/", appointmentList, name="appointmentList"),
    path("appointments/<int:pk>/", appointmentDetail, name="appointmentDetail"),
    path("appointments/<int:pk>/cancel/", appointmentCancel, name="appointmentCancel"),
    path("appointments/<int:pk>/finish/", appointmentFinish, name="appointmentFinish"),
    path("technicians/", technicianList, name="technicianList"),
    path("technicians/<int:pk>/", technicianDetail, name="technicianDetail"),
    path("automobiles/", autoMobileList, name="autoMobileList"),
]
