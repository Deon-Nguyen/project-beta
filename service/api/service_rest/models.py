from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=200, unique=True)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return self.vin


class Technician(models.Model):
    employee_id = models.CharField(max_length=100, null=True, unique=True)
    first_name = models.CharField(max_length=100, null=True)
    last_name = models.CharField(max_length=100, null=True)

    def __str__(self):
        return self.first_name

    def get_api_url(self):
        return reverse("technicianDetail", kwargs={"pk": self.id})


class Appointment(models.Model):
    STATUS_CHOICES = (
        ('created', 'Created'),
        ('canceled', 'Canceled'),
        ('finished', 'Finished'),
    )
    date_time = models.DateTimeField(null=True)
    reason = models.CharField(max_length=200, null=True)
    status = models.CharField(max_length=20, choices=STATUS_CHOICES, default='created')
    vin = models.CharField(max_length=200, null=True)
    customer = models.CharField(max_length=100, null=True)
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.vin

    def get_api_url(self):
        return reverse("appointmentDetail", kwargs={"pk": self.id})
