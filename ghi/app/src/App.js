import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import TechnicianList from './Service/Technician/TechnicianList';
import TechnicianForm from './Service/Technician/TechnicianForm';
import ServiceList from './Service/Appointment/ServiceList';
import ServiceForm from './Service/Appointment/ServiceForm';
import ServiceHistory from './Service/Appointment/ServiceHistory';
import ManufacturerList from './Inventory/Manufacturer/ManufacturerList';
import ManufacturerForm from './Inventory/Manufacturer/ManufacturerForm';
import ModelList from './Inventory/CarModel/ModelList';
import ModelForm from './Inventory/CarModel/ModelForm';
import AutomobileList from './Inventory/Automobile/AutomobileList';
import AutomobileForm from './Inventory/Automobile/AutomobileForm';
import ListSalesperson from './Sales/ListSalespeople';
import ListCustomer from './Sales/ListCustomers';
import ListSales from './Sales/ListSales';
import CustomerForm from './Sales/CustomerForm';
import SalesForm from './Sales/SalesForm';
import SalespersonForm from './Sales/SalesPersonForm';
import Nav from './Nav';
import SalespersonHistory from './Sales/SalesPersonHistory';



function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
            <Route path="technician"> ServiceForm
                <Route index element={<TechnicianList />} />
                <Route path="form" element={<TechnicianForm />} />
            </Route>
            <Route path="service">
                <Route index element={<ServiceList />} />
                <Route path="form" element={<ServiceForm />} />
                <Route path="history" element={<ServiceHistory />} />
            </Route>
            <Route path="manufacturers">
                <Route index element={<ManufacturerList />} />
                <Route path="form" element={<ManufacturerForm />} />
            </Route>
            <Route path="models">
                <Route index element={<ModelList />} />
                <Route path="form" element={<ModelForm />} />
            </Route>
            <Route path="automobiles">
                <Route index element={<AutomobileList />} />
                <Route path="form" element={<AutomobileForm />} />
            </Route>
            <Route path="salespeople">
                <Route index element={<ListSalesperson />} />
                <Route path="form" element={<SalespersonForm />} />
            </Route>
            <Route path="customers">
                <Route index element={<ListCustomer />} />
                <Route path="form" element={<CustomerForm />} />
            </Route>
            <Route path="sales">
                <Route index element={<ListSales />} />
                <Route path="history" element={<SalespersonHistory />} />
                <Route path="form" element={<SalesForm />} />
            </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
