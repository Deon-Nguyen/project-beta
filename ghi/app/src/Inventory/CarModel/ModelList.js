import {useState, useEffect} from 'react';

function ModelList() {
    const [models, setModels] = useState([]);

    async function loadModels() {
        const response = await fetch('http://localhost:8100/api/models/');
        const data = await response.json();
        setModels(data.models);
    }

    useEffect(() => {
        loadModels();
    }, []);



return (
    <div className="mt-3">
        <h1>Models</h1>
    <table className="table table-striped">
        <thead className='mt-5'>
        <tr>
            <th>Name</th>
            <th>Manufacturer</th>
            <th>Picture</th>
        </tr>
        </thead>
        <tbody>
        {models.map(model => {
            return (
            <tr key={ model.id }>
                <td>{ model.name }</td>
                <td>{ model.manufacturer.name }</td>
                <td><img alt="new" src={ model.picture_url }/></td>
            </tr>
            );
        })}
        </tbody>
    </table>
</div>
);
}
export default ModelList;
