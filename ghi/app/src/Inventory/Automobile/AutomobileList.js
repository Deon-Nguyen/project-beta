import {useState, useEffect} from 'react';

function AutomobileList() {
    const [automobiles, setAutomobiles] = useState([]);

    async function loadAutomobiles() {
        const response = await fetch('http://localhost:8100/api/automobiles/');
        const data = await response.json();
        setAutomobiles(data.autos);
    }

    useEffect(() => {
        loadAutomobiles();
    }, []);



return (
    <div className="mt-3">
        <h1>Automobiles</h1>
    <table className="table table-striped">
        <thead className='mt-5'>
        <tr>
            <th>VIN</th>
            <th>Color</th>
            <th>Year</th>
            <th>Model</th>
            <th>Manufacturer</th>
            <th>Sold</th>
        </tr>
        </thead>
        <tbody>
        {automobiles.map(auto => {
            return (
            <tr key={ auto.id }>
                <td>{ auto.vin }</td>
                <td>{ auto.color }</td>
                <td>{ auto.year }</td>
                <td>{ auto.model.name }</td>
                <td>{ auto.model.manufacturer.name }</td>
                <td>{ auto.sold ? "Yes":"No" }</td>
            </tr>
            );
        })}
        </tbody>
    </table>
</div>
);
}
export default AutomobileList;
