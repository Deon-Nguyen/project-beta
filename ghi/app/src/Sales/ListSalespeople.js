import {useState, useEffect} from 'react'

function ListSalesperson() {
    const [salespersons, setSalespersons] = useState([]);

    async function loadSalespersons() {
        const response = await fetch ("http://localhost:8090/api/salespeople/")
        const data = await response.json();
        setSalespersons(data.salespersons)
    }

    useEffect(() => {
        loadSalespersons();
    }, []);

    
return (
    <div>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Employee ID</th>
                </tr>
            </thead>
            <tbody>
            {salespersons.map(salesperson => {
            return (
            <tr key={salesperson.id}>
                <td>{ salesperson.first_name }</td>
                <td>{ salesperson.last_name }</td>
                <td>{ salesperson.employee_id }</td>
            </tr>
        );
    })}
            </tbody>
        </table>
    </div>
)
}

export default ListSalesperson;
