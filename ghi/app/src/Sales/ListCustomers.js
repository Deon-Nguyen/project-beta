import {useState, useEffect} from 'react'

function ListCustomer() {
    const [customers, setCustomer] = useState([]);

    async function loadCustomer() {
        const response = await fetch ("http://localhost:8090/api/customers/")
        const data = await response.json();
        setCustomer(data.customers)
    }

    useEffect(() => {
        loadCustomer();
    }, []);


return (
    <div>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Phone Number</th>
                    <th>Address</th>
                </tr>
            </thead>
            <tbody>
            {customers.map(customers => {
            return (
            <tr key={customers.id}>
                <td>{ customers.first_name }</td>
                <td>{ customers.last_name }</td>
                <td>{ customers.phone_number }</td>
                <td>{ customers.address }</td>
            </tr>
        );
    })}
            </tbody>
        </table>
    </div>
)

}

export default ListCustomer;
