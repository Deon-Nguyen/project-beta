import React, { useState, useEffect } from 'react';

function SalesForm() {
    const [customers, setCustomers] = useState([])
    const [autos, setAutos] = useState([])
    const [salespeople, setSalespeople] = useState([])
    const [formData, setFormData] = useState({
        automobile: '',
        salesperson: '',
        customer: '',
        price: '',
    });


    async function loadAutos() {
        const response = await fetch('http://localhost:8100/api/automobiles/');
        const data = await response.json();
        setAutos(data.autos);
    }

    async function loadSalespeople() {
        const response = await fetch('http://localhost:8090/api/salespeople/');
        const data = await response.json();
        setSalespeople(data.salespersons);
    }

    async function loadCustomers() {
        const response = await fetch('http://localhost:8090/api/customers/');
        const data = await response.json();
        setCustomers(data.customers);
    }

    useEffect(() => {
        loadCustomers();
        loadSalespeople();
        loadAutos();
    }, []);


    const handleSubmit = async (event) => {
        event.preventDefault();

        const salesUrl = 'http://localhost:8090/api/sales/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(salesUrl, fetchConfig);
        if (response.ok) {
            setFormData({
                automobile: '',
                salesperson: '',
                customer: '',
                price: '',
            });
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    }
    return (
        <div className="row">
            <div className="offset-3 col-5">
                <div className="shadow p-4 mt-4">
                    <div className="col-12 mb-3 mt-0 pt-0" >
                    <h1>Record a new sale</h1>
                    </div>
                    <form onSubmit={handleSubmit} id="create-sale-form">
                        <div className="col-12 mt-3">
                            <label htmlFor="automobile">Automobile VIN</label>
                            <select value={formData.automobile} onChange={handleFormChange} required name="automobile" id="automobile" className="form-select">
                            <option>Choose an automobile VIN...</option>
                            {autos.map(auto => {
                                return (
                                <option key={auto.id} value={auto.id} >
                                {auto.vin}
                                </option>
                                );
                            })}
                            </select>
                        </div>
                        <div className="col-12 mt-3">
                            <label htmlFor="salesperson">Salesperson</label>
                            <select value={formData.salesperson} onChange={handleFormChange} required name="salesperson" id="salesperson" className="form-select">
                            <option>Choose a salesperson...</option>
                            {salespeople.map(salesperson => {
                                return (
                                <option key={salesperson.id} value={salesperson.id} >
                                {salesperson.first_name}
                                </option>
                                );
                            })}
                            </select>
                        </div>
                        <div className="col-12 mt-3">
                            <label htmlFor="customer">Customer</label>
                            <select value={formData.customer} onChange={handleFormChange} required name="customer" id="customer" className="form-select">
                            <option>Choose a customer...</option>
                            {customers.map(customer => {
                                return (
                                <option key={customer.id} value={customer.id} >
                                {customer.first_name}
                                </option>
                                );
                            })}
                            </select>
                        </div>
                        <div className="col-12 mt-2">
                            <label htmlFor="price">Price</label>
                            <input placeholder="0" className="form-control" onChange={handleFormChange} value={formData.price} required type="number" name="price" id="price"/>
                        </div>
                        <button className="btn btn-primary mt-4">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}
export default SalesForm;
