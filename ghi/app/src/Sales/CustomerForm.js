import React, { useState } from 'react';

function CustomerForm() {
    const [formData, setFormData] = useState({
        first_name: '',
        last_name: '',
        address: '',
        phone_number: '',
    });

    const handleSubmit = async (event) => {
        event.preventDefault();

        const customerUrl = 'http://localhost:8090/api/customers/';

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(customerUrl, fetchConfig);

        if (response.ok) {
            setFormData({
                first_name: '',
                last_name: '',
                address: '',
                phone_number: '',
            });
        }

    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    }
    return (
        <div className="row">
            <div className="offset-3 col-5">
                <div className="shadow p-4 mt-4">
                    <div className="col-12 mb-3 mt-0 pt-0" >
                    <h1>Add a Customer</h1>
                    </div>
                    <form onSubmit={handleSubmit} id="create-customer-form">
                        <div className="col-12">
                            <input placeholder="First Name..." className="form-control" onChange={handleFormChange} value={formData.first_name} required type="text" name="first_name" id="first_name"/>
                        </div>
                        <div className="col-12 mt-2">
                            <input placeholder="Last Name..." className="form-control" onChange={handleFormChange} value={formData.last_name} required type="text" name="last_name" id="last_name"/>
                        </div>
                        <div className="col-12 mt-2">
                            <input placeholder="Address..." className="form-control" onChange={handleFormChange} value={formData.address} required type="text" name="address" id="address"/>
                        </div>
                        <div className="col-12 mt-2">
                            <input placeholder="Phone Number..." className="form-control" onChange={handleFormChange} value={formData.phone_number} required type="number" name="phone_number" id="phone_number"/>
                        </div>
                        <button className="btn btn-primary mt-4">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}
export default CustomerForm;
