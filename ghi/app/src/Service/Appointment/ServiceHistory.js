import {useState, useEffect, useCallback} from 'react';

function ServiceHistory() {
    const [appointments, setAppointments] = useState([]);
    const [autoMobiles, setAutoMobiles] = useState([]);
    const [search, setSearch] = useState("");
    const [searchClicked, setSearchClicked] = useState(false);
    const filteredAppointments = appointments.filter(appt => { return appt.status !== "created"})


    async function loadAppointments() {
        const response = await fetch('http://localhost:8080/api/appointments/');
        const data = await response.json();
        setAppointments(data.appointments);
    }
    async function loadAutoMobiles() {
        const response = await fetch('http://localhost:8080/api/automobiles/');
        const data = await response.json();
        setAutoMobiles(data.automobiles);
    }

    useEffect(() => {
        loadAutoMobiles();
        loadAppointments();
    }, []);


    const filterServices = useCallback(async () => {
    if (search !== "") {
        const response = await fetch('http://localhost:8080/api/appointments/');
        const data = await response.json();
        const filteredServices = data.appointments.filter(appt =>
            appt.vin.toLowerCase().includes(search.toLowerCase())
        );
        setAppointments(filteredServices);
    }
    }, [search]);

    useEffect(() => {
        filterServices();
        setSearchClicked(false)
    }, [searchClicked, filterServices]);



    function HandleSearchChange(e) {
        setSearch(e.target.value)
    }


    function HandleSearchClick() {
        setSearchClicked(true)
    }



return (
    <div className="mt-3">
        <h1>Service History</h1>
        <div className="col-12 mt-3 d-flex align-items-center">
            <input placeholder="Search by VIN..." className="form-control" onChange={HandleSearchChange} value={search} required type="text" name="search" id="search"/>
            <button onClick={HandleSearchClick}className="btn btn-light ml-2">Search</button>
        </div>
    <table className="table table-striped">
        <thead className='mt-5'>
        <tr>
            <th>VIN</th>
            <th>Is VIP?</th>
            <th>Customer</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>Status</th>
        </tr>
        </thead>
        <tbody>
        {filteredAppointments.map(appointment => {
            return (
            <tr key={appointment.id}>
                <td>{ appointment.vin }</td>
                <td>{autoMobiles.some(auto => auto.vin === appointment.vin) ? "Yes":"No" }</td>
                <td>{ appointment.customer}</td>
                <td>{ new Date(appointment.date_time).toLocaleDateString('en-US')}</td>
                <td>{ new Date(appointment.date_time).toLocaleTimeString('en-US', { hour: '2-digit', minute: '2-digit' })}</td>
                <td>{ appointment.technician.first_name }</td>
                <td>{ appointment.reason}</td>
                <td>{ appointment.status}</td>
            </tr>
            );
        })}
        </tbody>
    </table>
</div>
);
}
export default ServiceHistory;
