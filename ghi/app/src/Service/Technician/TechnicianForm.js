import React, { useState } from 'react';

function TechnicianForm() {
    const [formData, setFormData] = useState({
        employee_id: '',
        first_name: '',
        last_name: '',
    });

    const handleSubmit = async (event) => {
        event.preventDefault();


        const technicianUrl = 'http://localhost:8080/api/technicians/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };


        const response = await fetch(technicianUrl, fetchConfig);
        if (response.ok) {
            setFormData({
                employee_id: '',
                first_name: '',
                last_name: '',
            });
        }
    }


    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    }

return (
    <div className="row">
        <div className="offset-3 col-5">
            <div className="shadow p-4 mt-4">
                <div className="col-12 mb-3 mt-0 pt-0" >
                <h1>Add a Technician</h1>
                </div>
                <form onSubmit={handleSubmit} id="create-technician-form">
                    <div className="col-12">
                        <input placeholder="First Name..." className="form-control" onChange={handleFormChange} value={formData.first_name} required type="text" name="first_name" id="first_name"/>
                    </div>
                    <div className="col-12 mt-2">
                        <input placeholder="Last Name..." className="form-control" onChange={handleFormChange} value={formData.last_name} required type="text" name="last_name" id="last_name"/>
                    </div>
                    <div className="col-12 mt-2">
                        <input placeholder="Employee ID..." className="form-control" onChange={handleFormChange} value={formData.employee_id} required type="text" name="employee_id" id="employee_id"/>
                    </div>
                    <button className="btn btn-primary mt-4">Create</button>
                </form>
            </div>
        </div>
    </div>
);
}
export default TechnicianForm;
