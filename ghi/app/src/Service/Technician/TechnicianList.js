import {useState, useEffect} from 'react';

function TechnicianList() {
    const [technicians, setTechnicians] = useState([]);


    async function loadTechnicians() {
        const response = await fetch('http://localhost:8080/api/technicians/');
        const data = await response.json();
        setTechnicians(data.technicians);
    }

    useEffect(() => {
        loadTechnicians();
    }, []);


return (
    <div className="mt-3">
        <h1>Technicians</h1>
    <table className="table table-striped">
        <thead className='mt-5'>
        <tr>
            <th>Employee ID</th>
            <th>First Name</th>
            <th>Last Name</th>
        </tr>
        </thead>
        <tbody>
        {technicians.map(technician => {
            return (
            <tr key={technician.id}>
                <td>{ technician.employee_id }</td>
                <td>{ technician.first_name }</td>
                <td>{ technician.last_name }</td>
            </tr>
            );
        })}
        </tbody>
    </table>
</div>
);
}
export default TechnicianList;
