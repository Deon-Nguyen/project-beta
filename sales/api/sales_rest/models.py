from django.db import models


class AutomobileVO(models.Model):
    id = models.IntegerField(primary_key=True)
    vin = models.CharField(max_length=200, unique=True)
    sold = models.BooleanField(default=False)


class Salesperson(models.Model):
    first_name = models.CharField(max_length=200, null=True, blank=True)
    last_name = models.CharField(max_length=200, null=True, blank=True)
    employee_id = models.CharField(max_length=200, null=True, unique=True)


class Customer(models.Model):
    first_name = models.CharField(max_length=200, null=True, blank=True)
    last_name = models.CharField(max_length=200, null=True, blank=True)
    address = models.CharField(max_length=200, null=True, blank=True)
    phone_number = models.IntegerField(null=True, blank=True)


class Sale(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="AutoSale",
        on_delete=models.CASCADE,
        null=True,
    )
    salesperson = models.ForeignKey(
        Salesperson,
        related_name="SalesPersonSale",
        on_delete=models.CASCADE,
        null=True,

    )
    customer = models.ForeignKey(
        Customer,
        related_name="CustomerSale",
        on_delete=models.CASCADE,
        null=True,
    )
    price = models.FloatField(null=True)
